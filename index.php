<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <script src="jquery.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 bg-info p-5 mx-auto text-white">
                <h1 class="text-center">Add User</h1>
                <hr/>
                <?php 
                    $connect = new mysqli("localhost",'root','','abcd');
                    if(isset($_POST['firstname'])){
                        $firstname = $_POST['firstname'];
                        $lastname = $_POST['lastname'];
                        $gender = $_POST['gender'];
                        $class = $_POST['class'];
                        $year = $_POST['year'];
                        $percentage = $_POST['percentage'];
                        $qry = "INSERT INTO users (firstname, lastname, gender) VALUES ('$firstname','$lastname','$gender')";
                        if($connect->query($qry)){
                            $id = $connect->insert_id;
                            $qry1 = "INSERT INTO education (userid, class, passyear, percentage) VALUES ('$id','$class','$year','$percentage')";
                            if($connect->query($qry1)){
                                echo "User Inserted";
                            }
                        }
                    }
                ?>
                <form action="manage.php" method="post">
                    <label>First Name</label>
                    <input type="text" class="form-control" name="firstname" />
                    <label>Last Name</label>
                    <input type="text" class="form-control" name="lastname"/>
                    <label>Gender</label>
                    <select class="form-control" name="gender">
                        <option>Male</option>
                        <option>Female</option>
                    </select>
                    <div class="education">
                        <div class="row mt-3">
                            <div class="col-md-4">
                                <label>Class</label>
                                <input type="text" name="class[]" class="form-control" />
                            </div>
                            <div class="col-md-4">
                                <label>Pass Year</label>
                                <input type="text" class="form-control" name="year[]"/>
                            </div>
                            <div class="col-md-4">
                                <label>Percentage</label>
                                <input type="text" class="form-control" name="percentage[]" />
                            </div>
                        </div>
                    </div>
                    <button type="button" onclick="addField()">Add</button>
                    <input type="submit" class="btn btn-success btn-block mt-3" />
                </form>
            </div>
        </div>
    </div>
    <script>
        let data = $('.education').html();
        function addField(){
            $('.education').append(data);
        }
    </script>
</body>
</html>
