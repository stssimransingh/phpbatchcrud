<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <script src="jquery.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 bg-info p-5 mx-auto text-white">
                <h1 class="text-center">Add User</h1>
                <hr/>
                <form action="">
                    <label>First Name</label>
                    <input type="text" class="form-control" />
                    <label>Last Name</label>
                    <input type="text" class="form-control" />
                    <label>Gender</label>
                    <select class="form-control">
                        <option>Male</option>
                        <option>Female</option>
                    </select>
                    <div class="educations">
                        <div class="row mt-3" id='education'>
                            <div class="col-md-4">
                                <label>Class</label>
                                <input type="text" class="form-control" />
                            </div>
                            <div class="col-md-4">
                                <label>Pass Year</label>
                                <input type="text" class="form-control" />
                            </div>
                            <div class="col-md-4">
                                <label>Percentage</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <button class="abcd btn btn-success mt-3" type="button" >Add More</button>
                    <button class="remove btn btn-danger mt-3" type="button" >Remove</button>
                    <input type="submit" class="btn btn-success btn-block mt-3" />
                </form>
            </div>
        </div>
    </div>
    <script>
    let m =  '<div class="row mt-1" id="education">';
                m += '<div class="col-md-4">';
                    m +='<label>Class</label>';
                    m += '<input type="text" class="form-control" />';
                    m += '</div>';
                    m += '<div class="col-md-4">';
                    m +='<label>Pass Year</label>';
                    m +='<input type="text" class="form-control" />';
                    m +='</div>';
                    m +='<div class="col-md-4">';
                    m +='<label>Percentage</label>';
                    m +='<input type="text" class="form-control" />';
                    m +='</div>';
                    m +='</div>';
        $(function(){
            let a = $('#education').html();
            $('.abcd').click(function(){
                $('.educations').append(m);
            })
        })
    </script>
</body>
</html>
